from django.http import HttpResponse
from django.shortcuts import render
from django.shortcuts import render
from django.db import connections
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout

from .backend.fixture_generation import main
from .backend.config_data import config_json
from django.views.decorators.csrf import csrf_exempt
import json
# Create your views here.

def index(request):
    # return HttpResponse("Hello, world. You're at the polls index.")
    print('here index')
    title = "MyClub Event Calendar "
    cal = "ATHIRA"
    filen = "football_app/index.html"
    # return render(request, 'football_app/base.html', {'title': title, 'cal': cal, 'filen': filen})
    return render(request, 'football_app/index.html')
@csrf_exempt
def user_login(request):
    print('here login')
    if request.method == 'POST':
        username = request.POST.get('uname')
        password = request.POST.get('password')
        if (username=="admin" and password=="admin@123"):
            return HttpResponse(json.dumps({'res': "success"}),
                                content_type="application/json")
        else:
            return HttpResponse(json.dumps({'res': "error"}),
                                content_type="application/json")

@csrf_exempt
def registration(request):
    print('here registration')
    cursor = connections['default'].cursor()
    cursor.execute("select count(id) from registration")
    # make result in dict format
    fieldnames = [name[0] for name in cursor.description]
    result = []
    for row in cursor.fetchall():
        print(row[0])
        team_count = row[0]
    title = "Registration"
    if team_count<10:

        # filen = "football_app/registration.html"
        return render(request, 'football_app/registration.html')
    else:
        # filen = "football_app/registration_closed.html"
        return render(request, 'football_app/registration_closed.html')

    # return render(request, 'football_app/base.html',{'registration_status': 'active','title': title, 'filen': filen,'team_count':team_count})
@csrf_exempt
def register_function(request):
    print('here registration1')
    cursor = connections['default'].cursor()
    cursor.execute("select count(id) from registration")

    for row in cursor.fetchall():
        print(row[0])
        team_count = row[0]
    title = "Registration"
    if team_count < 10:

        team_name = request.POST.get('team_name')
        coach_name = request.POST.get('coach_name')
        manager_name = request.POST.get('manager_name')
        players = request.POST.get('members_name')
        print('fffffffffffffffff'+players)
        try:
            print('fffffffffffffffff')
            cursor = connections['default'].cursor()
            print(cursor)
            sql = "insert into  registration (team_name, coach_name, manager_name, players) values(%s, %s, %s, %s) "
            cursor.execute(sql, (team_name, coach_name, manager_name, players))
            connections['default'].commit()
            # cursor = connections['default'].cursor()
            cursor.execute("select count(id) from registration")
            # make result in dict format
            fieldnames = [name[0] for name in cursor.description]
            result = []
            for row in cursor.fetchall():
                print(row[0])
                team_count = row[0]
            title = "Registration"
            if team_count == 10:

                val = main()
                print('val');
                print(type(val));
                for row in val:
                    print(row)
                    team = row[0]
                    venue = row[1]
                    date = row[2]
                    cursor = connections['default'].cursor()
                    #     print(cursor)
                    sql = "insert into  fixtures (teams, venue, date) values(%s, %s, %s) "
                    cursor.execute(sql, (str(team), str(venue), date))
                    connections['default'].commit()
                    connections['default'].close()
                    val = "Registration closed-fixtures ready"

            else:
                val = "Successfully Registered"
            connections['default'].close()
        except Exception as exception:
            val = str(exception)
    else:
        val = "Registration closed-fixtures ready"
    return HttpResponse(json.dumps({'res': val}), content_type="application/json")
@csrf_exempt
def squad(request):
    # print('inside squad')
    cursor = connections['default'].cursor()
    cursor.execute("select id,team_name, coach_name, manager_name, players from registration")
    fieldnames = [name[0] for name in cursor.description]
    result = []
    for row in cursor.fetchall():
        row_set = []
        for field in zip(fieldnames, row):
            row_set.append(field)
        result.append(dict(row_set))
    connections['default'].close()
    config_json.update({"teamdata": result})

    filen = "football_app/squad.html"
    title = "SQUAD DETAILS"
    return render(request, 'football_app/base.html',  {'home_status': 'active','config_json':config_json,'title': title,'filen': filen})


def fixtures(request):
    print('inside fixtures')
    cursor = connections['default'].cursor()
    cursor.execute("select id,teams, venue, date, winner_name, winner_score, loser_score from fixtures")
    fieldnames = [name[0] for name in cursor.description]
    result = []
    for row in cursor.fetchall():
        row_set = []
        for field in zip(fieldnames, row):
            row_set.append(field)
        result.append(dict(row_set))
    # now result is in dict format with header and row
    connections['default'].close()
    config_json.update({"trainData": result})
    filen = "football_app/fixtures.html"
    title = "FIXTURES AND GOAL SCORE"
    return render(request, 'football_app/base.html',  {'training_status': 'active','config_json':config_json,'title': title,'filen': filen})






@csrf_exempt
def update_result(request):
    print("inside delete header")
    id = request.POST.get('id')
    winner_name = request.POST.get('winner_name')
    winner_score = request.POST.get('winner_score')
    loser_score = request.POST.get('loser_score')
    try:
        cursor = connections['default'].cursor()
        cursor.execute('UPDATE fixtures SET winner_name='+ winner_name+',winner_score='+ winner_score+',loser_score='+ loser_score+' WHERE id=%s'% id)
        connections['default'].commit()
        connections['default'].close()
        val = "HEADER SUCCESSFULLY DELETED"
    except Exception as exception:
        val = str(exception)
    return HttpResponse(json.dumps({'res':val}), content_type="application/json")
