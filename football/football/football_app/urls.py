from django.urls import path

from . import views
from django.conf.urls import url

urlpatterns = [
    path('', views.index, name='index'),
    path('fixtures', views.fixtures, name='fixtures'),
    path('update_result', views.update_result, name='update_result'),
    path('squad', views.squad, name='squad'),
    path('user_login', views.user_login, name='user_login'),
    path('registration', views.registration, name='registration'),
    path('register_function', views.register_function, name='register_function'),

]
