from pprint import pprint as pp

def make_day(num_teams, day):
    # using circle algorithm, https://en.wikipedia.org/wiki/Round-robin_tournament#Scheduling_algorithm
    assert not num_teams % 2, "Number of teams must be even!"
    # generate list of teams
    lst = list(range(1, num_teams + 1))
    # rotate
    day %= (num_teams - 1)  # clip to 0 .. num_teams - 2
    if day:                 # if day == 0, no rotation is needed (and using -0 as list index will cause problems)

        lst = lst[:1] + lst[-day:] + lst[1:-day]

    # pair off - zip the first half against the second half reversed
    half = num_teams // 2
    return list(zip(lst[:half], lst[half:][::-1]))

def make_schedule(num_teams):
    """
    Produce a double round-robin schedule
    """
    # number of teams must be even
    if num_teams % 2:
        num_teams += 1  # add a dummy team for padding
    #schedule1=[]
    # build first round-robin
    schedule = [make_day(num_teams, day) for day in range(num_teams - 1)]
    #for day in range(num_teams - 1):
        #schedule1.append(make_day(num_teams, day))

    # generate second round-robin by swapping home,away teams
    swapped = [[(away, home) for home, away in day] for day in schedule]

    return schedule

def main():
    num_teams = 10
    schedule = make_schedule(num_teams)
    pp(schedule)
    schedule1=[]
    r=[]
    for a in schedule:
        for s in a:
            schedule1.append(s)
    print(schedule1)
    venueArr=['venue1','venue2','venue3','venue4','venue5']

    from dateutil import rrule
    from datetime import datetime

    a = '20200801'
    b = '20200823'
    dateArr=[]
    for dt in rrule.rrule(rrule.DAILY,
                          dtstart=datetime.strptime(a, '%Y%m%d'),
                          until=datetime.strptime(b, '%Y%m%d')):

        dateArr.append(dt.strftime('%Y-%m-%d'))

    for i,venue in enumerate(schedule1):


        r.append([venue,venueArr[i%5],dateArr[int(i)//2]])

    return r;
if __name__ == "__main__":
    main()
