function editresult(id,teams,venue,date,wn,ws,ls) {
    if (wn=='None'){
        wn='';
    }
    if (ws=='None'){
        ws='';
    }
    if (ls=='None'){
        ls='';
    }

    $("#abc").css("display","block");
    $("#fixtures").css("filter","opacity(30%)");
    var inputid = document.getElementById("id");
    var teams_played = document.getElementById("teams_played");
    var input_venue= document.getElementById("venue");
    var date_of_match = document.getElementById("date_of_match");
    var winner_name = document.getElementById("winner_name");
    var winner_score = document.getElementById("winner_score");
    var loser_score = document.getElementById("loser_score");
    inputid.value = id;
    teams_played.value = teams;
    input_venue.value = venue;
    date_of_match.value = date;
    winner_name.value=wn;
    winner_score.value=ws;
    loser_score.value=ls;
}
$(document).ready(function() {

  $('#traingdataset').DataTable({
    "pagingType": "full_numbers"
  });
  $('.dataTables_length').addClass('bs-select');



    $('#update').click(function(){
        var id   = $('#id').val();
        var winner_name   = $('#winner_name').val();
        var winner_score = $('#winner_score').val();
        var loser_score = $('#loser_score').val();
        var resultDetails = new FormData();
        resultDetails.append("id",id);
        resultDetails.append("winner_name",winner_name);
        resultDetails.append("winner_score",winner_score);
        resultDetails.append("loser_score",loser_score);
        $.ajax({
            type:"POST",
            url:"update_result",
            data:resultDetails,
            cache: false,
            contentType: false,
            processData: false,
            success: function(result){
//                alert('please refresh the page to see the updated result');
                location.reload();
                $("#abc").css("display","none");
                $("#fixtures").css("filter","opacity(100%)");
                location.reload();
            }
        });
    });





});

