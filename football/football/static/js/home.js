//annotation starts here
$(document).ready(function() {
//    variables
    $(".otherState").val('');
    $(".otherCounty").val('');
    var testingState = '';
    var testingCounty = '';
    var sellerOrBuyer = '';
    var selectState =[];
    var selectCounty = [];
    var selectLabel='';
    var currentState='';
    var currentCounty='';

//    functions
    $("#annotation_form").submit(function(event){alert("success")
        event.preventDefault();
        var finalData = $("#hid1").val();
        console.log(finalData)
        if (finalData) {
            $.ajax({
                type:"POST",
                url:"annotation",
                data:finalData,
                contentType: "application/json",
                success: function(result){
                    alert(result.res);
                    console.log(result.res);
                }
            });
        }
    });

    $("#addLabel").click(function(){
        var textComponent = document.getElementById('main_text');
        var field   = $('#datafield').val();
        var fullVal = $('#main_text').val();
        var docType = $('#doc_type').val();
        var doc_heading = $('#doc_heading').val();
        var labelType = $('[name="label_type"]').val();
        var stateName = $('#state').val();
        var countyName = $('#county').val();
        if (textComponent.selectionStart !== undefined)
        {
            var startPos = textComponent.selectionStart;
            var endPos = textComponent.selectionEnd;
            if(endPos == 0) {
                alert('Please select a portion');
            }
            if(!labelType) {
                alert('Please add a label');
            }
            if (endPos > 0 && fullVal && (endPos != startPos)) {
                var someData = {start : startPos, end : endPos, label : labelType, fullData : fullVal, document : docType, doc_header : doc_heading, state : stateName, county : countyName};
                var jsonString = JSON.stringify(someData);
                if($("#hid1").val()) {
                    $("#hid1").val($("#hid1").val() +','+ jsonString);
                } else {
                    $("#hid1").val(jsonString);
                }
                $("#displayPart").append('<p>'+labelType+'</p>');
                document.getElementById("clickButton").disabled = false;
                alert("Added "+labelType);
            }
        }
    });

    $("#refresh").click(function(){
        var fullVal = $('#main_text').val();
        fullVal = fullVal.replace(/(\s\n|\n\s|\n)/g, " ");
        var filterVal = fullVal.replace(/[^A-Za-z 0-9 \.,\?""!@#\$%\^&\*\(\)-_=\+;:<>\/\\\|\}\{\[\]`~]*/g, '') ;
        document.getElementById('main_text').value = filterVal;
    });
//annotation ends here

//training starts here
    $("#trainForm").submit(function(event){
        var i,j;
        var eachState;
        var eachCounty;
        var count =0;
        $('input[name="check"]:visible').prop('checked', false);
        console.log(selectState);
        console.log(selectCounty);
        console.log(selectLabel);
        var totalRowCount = document.getElementById("grid").getElementsByTagName("tbody")[0].getElementsByTagName("tr").length;
        var checkId = document.getElementsByName('check');
        if(selectState.length !== 0 && selectCounty.length !== 0 && selectLabel !==''){
            for( j = 0 ;j< selectCounty.length ;j++ ){
                for( i = 1; i<=totalRowCount;i++){
                    eachCounty = document.getElementById("grid").rows[i].cells[6].innerHTML.replace(/_/g, ' ').toLowerCase();
                    eachState  = document.getElementById("grid").rows[i].cells[5].innerHTML.toLowerCase();
                    labelSel   = document.getElementById("grid").rows[i].cells[4].innerHTML.toLowerCase();
                    if(eachState ==selectState[j].toLowerCase() && eachCounty ==selectCounty[j].toLowerCase() && labelSel== selectLabel.toLowerCase()){
                        checkId[i-1].checked=true;
                        console.log("success");
                        count++;
                    }
                }
            }
            $(".lead").removeClass("modelError");
            $(".lead").addClass("modelSuccess");
            $( ".modal-title" ).html( 'Selected' );
            $( ".lead" ).html( count+' records Selected' );
        }else{
            $(".lead").removeClass("modelSuccess");
            $(".lead").addClass("modelError");
            $( ".modal-title" ).html( 'Input Missing' );
            $( ".lead" ).html('Please Select the State ,County and Label');
        }
        event.preventDefault();
    });

    $(".searchDataBtn").click(function (){
        var value = $('#searchData').val().toLowerCase();
        if(selectLabel == ''){
            $("#successBtn").click();
            $(".lead").removeClass("modelSuccess");
            $(".lead").addClass("modelError");
            $( ".modal-title" ).html( 'Input Missing' );
            $( ".lead" ).html( 'Please Select the Label' );
        }else{
            if(value.length){
                $("table tr").each(function (index){
                    if(index != 0){
                        $row = $(this);
                        var sorb=$row.find("td").eq(4).text();
                        $row.find("td").eq(3).each(function (){
                            var cell = $(this).text().toLowerCase();
                            if(cell.indexOf(value) < 0){
                                $row.hide();
                            }else if(sorb == selectLabel ){
                                $row.show();
                                return false; //Once it's shown you don't want to hide it on the next cell
                            }else{
                                $row.hide();
                            }
                        });
                    }
                });
            }else{
            //if empty search text, show all rows
            $("table tr").show();
            }
        }
    });

    $('#trainDataFun').click(function(){
        $("#myDiv").css("display","block");
        $(".container").css("filter","opacity(30%)");
        var valueList = '';
        $('#grid tr').each(function(){
            $(this).find("input[name='check']:checked").each(function(){
                valueList += ","+$(this).closest("td").siblings("td:nth-child(3)").text();
            });
        });
        var totalCount = $('input[name="check"]:checked').length;
        console.log("total count-----"+totalCount);
        console.log(currentState+"-----"+currentCounty);
        var sendData = {'value_list':valueList,'current_state':currentState,'current_county':currentCounty,'label':selectLabel}
        sendData = JSON.stringify(sendData)
        if(currentState !== '' && currentCounty !=='' ){
            $.ajax({
                type:"POST",
                url:"train_data",
                data:sendData,
                contentType: "application/json",
                success: function(result){
                    if(result.res == "success"){
                        console.log(result.res);
                        alert("Model created successfully");
                        $(".lead").removeClass("modelError");
                        $(".lead").addClass("modelSuccess");
                        $(".lead" ).html( 'Model created successfully');
                        $(".modal-title" ).html( 'Training Completed');
                        location.reload()
                    }else{
                        $(".lead").removeClass("modelSuccess");
                        $(".lead").addClass("modelError");
                        $( ".modal-title" ).html( 'ERROR!!!' );
                        $( ".lead" ).html( 'Mysql error occurred when training' );
                    }
//                    $("#successBtn").click();
                },
                error:function(xhr){
                    $("#successBtn").click();
                    $(".lead").removeClass("modelSuccess");
                    $(".lead").addClass("modelError");
                    $( ".modal-title" ).html( 'ERROR!!!' );
                    $( ".lead" ).html( 'Some Error occurred when training data' );
                },
                complete:function(){
                    $("#myDiv").css("display","none");
                    $(".container").css("filter","opacity(100%)");
                }
            });
        }else{
            $("#successBtn").click();
            $(".lead").removeClass("modelSuccess");
            $(".lead").addClass("modelError");
            $( ".modal-title" ).html( 'Input Missing' );
            $( ".lead" ).html( 'Please enter the current State and County' );
            $("#myDiv").css("display","none");
            $(".container").css("filter","opacity(100%)");
        }
    });

    $("#trainSel1").change(function(){
        var echval =$(this).children("option:selected").val()
        selectState.push(echval);
        $("#states").append("<p class='ptag'><i class='fa fa-close closee' style='font-size:14px;color:red'></i>"+echval+"</p>");
        $('#trainSel1 option').prop('selected', function(){
            return this.defaultSelected;
        });
    });

    $("#trainSel2").change(function(){
        var echval =$(this).children("option:selected").val()
        selectCounty.push(echval);
        $("#counties").append("<p class='ptag'><i  class='fa fa-close closee' style='font-size:14px;color:red'></i>"+echval+"</p>");
        $('#trainSel2 option').prop('selected', function(){
            return this.defaultSelected;
        });
    });

    $("#counties").on('click',".ptag", function(){
        var val = $(this).text();
        var index = selectCounty.indexOf(val);
        console.log(index);
        console.log(val);
        if(index > -1){
            selectCounty.splice(index, 1);
        }
        $(this).remove();
    });

    $("#states").on('click',".ptag", function(){
        var val = $(this).text();
        var index = selectState.indexOf(val);
        console.log(index);
        console.log(val);
        selectState.splice(index, 1);
        $(this).remove();
    });

    $("#trainSel3").change(function(){
        var echval =$(this).children("option:selected").val();
        selectLabel=echval;
    });

    $("#currentState").change(function(){
        currentState =$(this).children("option:selected").val();
    });

    $("#currentCounty").change(function(){
        currentCounty =$(this).children("option:selected").val();
    });

    $('input[name="checkAll"]').click(function (){
        if($('input[name="checkAll"]').prop('checked')){
            $('input[name="check"]:visible').prop('checked', true);
        }else{
            $('input[name="check"]:visible').prop('checked', false);
        }
    });
//training ends here

//conversion starts here
    $("#upload_file").change(function(){
//        var url = $(this).attr("data-import-url");
        var data = new FormData();
        $.each($("#upload_file")[0].files, function(i, file){
            data.append("file", file);
        });
        var stateUpload   = $('.upload_state').val();
        var state = document.getElementById('upload_file').elements['state_up'].value;
        var countyUpload  = $('.uploadCounty').val();
        var batchUpload   = $('.batchUp').val() ;
        var filetypeUpload= $('#filetype').val() ;
        data.append("csrfmiddlewaretoken", $(this).attr("data-csrf-token"));
        data.append("state",state);
        data.append("county",countyUpload);
        data.append("batch",batchUpload);
        data.append("filetype",filetypeUpload);
        $.ajax({
            url: "conversion",
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'post',
            beforeSend: function(){
                // before send, display loading, etc...
                $("#myDiv").css("display","block");
                $(".container").css("filter","opacity(30%)");
            },
            success: function(data){
                console.log(data['state'],)
                alert('conversion completed')
//                window.location = 'download_file/?state='+data['state']+'&county='+data['county']+'&batch='+data['batch']+'&image='+data['image']+'&image_list='+data['image_list']+'&filetype='+data['filetype']
                // success handling...
            },
            error: function (jqXHR, exception){
                // error handling...
                console.log("Error");
                console.log(jqXHR);
            },complete:function(){
                $("#myDiv").css("display","none");
                $(".container").css("filter","opacity(100%)");
            }
        });
    });
});

