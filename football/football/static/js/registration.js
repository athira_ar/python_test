
$(document).ready(function() {
    $("#myDiv").css("display","block");
    $("#register").click(function(){
        var team_name = $('#team_name').val();
        var coach_name   = $('#coach_name').val();
        var manager_name = $('#manager_name').val();
        var members_name = $('#members_name').val();
        var data = new FormData();
        data.append("team_name",team_name);
        data.append("coach_name",coach_name);
        data.append("manager_name",manager_name);
        data.append("members_name",members_name);
//        for (var pair of data.entries()) {
//            console.log(pair[0]+ ', ' + pair[1]);
//        }
        $.ajax({
            type:"post",
            url:"register_function",
            data:data,
            cache: false,
            contentType: false,
            processData: false,
            success: function(result){
                alert(result.res);
                location.reload();
//                console.log(result.res);
            }
        });
    });
});
